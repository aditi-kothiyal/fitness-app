<?php
   require_once 'header.php';
   ?>
<div class="header header-fixed header-logo-center">
   <a href="index" class="header-title">Notifications</a>
   <a href="javascript:history.back()" class="header-icon header-icon-1"><i class="fa fa-arrow-left fa-lg"></i></a>
</div>
<div class="page-content header-clear-medium">
   <div class="container container-fluid">
      <div class="list-group list-custom-large">
         <a href="#">
            <img src="images/user-1.jpg" class="profile-img">
            <span>Jim in team KOLKATA won a new 7-Day Streak</span>
            <strong><i class="fa fa-users float-left mr-0" style="margin-left:-1vh;"></i>22 hours ago</strong>
         </a>
         <a href="#">
            <img src="images/user-1.jpg" class="profile-img">
            <span>Jim in team KOLKATA won a new 7-Day Streak</span>
            <strong><i class="fa fa-users float-left mr-0" style="margin-left:-1vh;"></i>22 hours ago</strong>
         </a>
         <a href="#">
            <img src="images/user-1.jpg" class="profile-img">
            <span>Jim in team KOLKATA won a new 7-Day Streak</span>
            <strong><i class="fa fa-users float-left mr-0" style="margin-left:-1vh;"></i>22 hours ago</strong>
         </a>
      </div>
   </div>
</div>
<?php
   require_once 'js-links.php';
   ?>