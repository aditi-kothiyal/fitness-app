<?php
   require_once 'header.php';

require 'config.php';




$stmt=$link->prepare("SELECT * FROM teams" );
        
        $stmt->execute();
        $result=$stmt->get_result();
        
if ($result->num_rows > 0) {
  // output data of each row
  while($row = $result->fetch_assoc())
  {
      $teams[]=$row;
  }
}
	
   ?>
<div class="header header-fixed header-logo-center">
   <a href="#" class="header-title">Join Team</a>
   <a href="javascript:history.back()" class="header-icon header-icon-1"><i class="fa fa-arrow-left fa-lg"></i></a>
</div>
<div class="page-content header-clear-small">
   <div class="content">
   <div class="search-box bg-white rounded-s mb-3 mt-5">
<i class="fa fa-search"></i>
<input type="text" class="border-0" placeholder="Search for the team you  want to join" data-search="">
</div>
      <div class="list-group list-custom-small">
             <?php
        if(isset($teams))
        {
            
            foreach($teams as $team)
            {
              
                ?>
         <a href="league-member-list">
            <img src="<?= $team['t_img']?>" class="profile-img">
            <?= $team['t_name']?>
            <span class="float-right"><?= $team['t_members']?> <i class="fa fa-users fa-lg" style="font-size:16px;color:black"></i></span>
         </a>
          <?php
            }
        }
          ?>
       <!--  <a href="league-member-list my-5">
            <img src="images/user-1.jpg" class="profile-img">
            Jim
            <span class="float-right">8 <i class="fa fa-users fa-lg" style="font-size:16px;color:black"></i></span>
         </a>
         <a href="league-member-list">
            <img src="images/user-1.jpg" class="profile-img">
            Mark
            <span class="float-right">8 <i class="fa fa-users fa-lg" style="font-size:16px;color:black"></i></span>
         </a>-->
      </div>
   </div>
</div>
<?php
   require_once 'footer.php';
   require_once 'js-links.php';
   ?>