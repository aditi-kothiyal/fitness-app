<?php
   require_once 'header.php';
   ?>
<div class="header header-fixed header-logo-center">
   <a href="#" class="header-title">Telengana</a>
   <a href="javascript:history.back()" class="header-icon header-icon-1"><i class="fa fa-arrow-left fa-lg"></i></a>
   <a href="#" class="header-icon header-icon-4"><i class="fa fa-sign-out-alt fa-lg"></i></a>
</div>
<div class="page-content header-clear-medium">
   <div class="content">
      <div class="list-group list-custom-small">
         <a href="league-member-list">
            <img src="images/user-1.jpg" class="profile-img">
            John
            <span class="float-right">Rs 9000</span>
         </a>
         <a href="league-member-list">
            <img src="images/user-1.jpg" class="profile-img">
            Jim
            <span class="float-right">Rs 10000</span>
         </a>
         <a href="league-member-list">
            <img src="images/user-1.jpg" class="profile-img">
            Mark
            <span class="float-right">Rs 11000</span>
         </a>
      </div>
   </div>
</div>
<?php
   require_once 'footer.php';
   require_once 'js-links.php';
   ?>