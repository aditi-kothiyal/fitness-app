<?php
   require_once 'header.php';
require_once 'config.php';

$first = $last =$phone = "";
$first_err = $last_err = $phone_err = "";

?>



<div class="page-content header-clear-medium">
         <form id="dataform" method="post">
   <div class="content">
      <div class='container container-fluid'>
        <h1 class="text-center mb-5">Enter Name & Email</h1>

        
            <span class="color-highlight input-style-1-inactive">First Name</span>
            <input class="form-control" type="text" placeholder="First Name" name="first" id="first" value="<?php echo $first; ?>">
                <span class="help-block"><?php echo $first_err; ?></span>
        
         
            <span class="color-highlight input-style-1-inactive">Last Name</span>
            <input class="form-control" type="text" placeholder="Last Name" name="last" id="last" value="<?php echo $last; ?>">
          <span class="help-block"><?php echo $last_err; ?></span>
          
            <span class="color-highlight input-style-1-inactive">Phone</span>
            <input class="form-control" type="tel" placeholder="Phone Number" name="phone" id="phone" value="<?php echo $phone; ?>">
                <span class="help-block"><?php echo $phone_err; ?></span>
          <p style="color:red" id="msg1"></p>
         
        <div class="form-group">
                <input type="submit" class="btn btn-m rounded-s text-uppercase font-700 mb-2 btn-center-xl bg-highlight w-100 mt-4" value="SIGN ME UP">
                
            </div>
      </div>
   </div>
    </form>
</div>
   

<?php
   require_once 'js-links.php';
   ?>


<script>
    $(document).ready(function(){
         $("#dataform").on('submit', function(e){
             var phone_number=$('#phone').val();
        	var re = /^[5-9]\d{9}$/;
	var condition = re.test(phone_number);
//	console.log(condition);	
	$('#msg1').html("");
	if(!condition){
		//$('#msg1').html(" * Invalid Phone Number");
		//document.getElementById("phoneNo").value="";
        alert("*invalid phone number");
	}
		       
    			e.preventDefault();
    			$.ajax({
    				type: 'POST',
    				url: 'update_data.php',
    				data: new FormData(this),
    				contentType: false,
    				cache: false,
    				processData:false,
    				success:function(data){
    				    var obj=JSON.parse(data);
                        
                        if(obj.msg=='ok')
                            {
                              window.location='login-4.php';
                            }
                       
                        else{
                            $('#msg1').html(obj.msg);
                        }
                      
    				},
                    
    			});
    });
    });
    
    
</script>
    


