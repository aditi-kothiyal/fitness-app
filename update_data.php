<?php

require_once 'config.php';

session_start();

if(isset($_POST['first']) && isset($_POST['last']) && isset($_POST['phone']))
{
    $phone=$_POST['phone'];
    $first=$_POST['first'];
    $last=$_POST['last'];
    $id=$_SESSION['loggin_data'];
    
    $stmt_u=$link->prepare("UPDATE user SET firstname=?,lastname=?,phone=? WHERE id=?");
    $stmt_u->bind_param("sssi",$first,$last,$phone,$id);
    
    if($stmt_u->execute())
    {
        $response['msg']='ok';
    }
    else
    {
        $response['msg']='phone no does not match';
    }
    
    echo json_encode($response);
}
?>          
